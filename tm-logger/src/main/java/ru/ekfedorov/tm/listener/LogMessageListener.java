package ru.ekfedorov.tm.listener;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.service.ILoggingService;
import ru.ekfedorov.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    @Autowired
    public ILoggingService loggingService;

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            loggingService.writeLog(message);
        }
    }

}

package ru.ekfedorov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.service.*;
import ru.ekfedorov.tm.command.AbstractCommand;
import ru.ekfedorov.tm.command.system.ExitCommand;
import ru.ekfedorov.tm.component.FileScanner;
import ru.ekfedorov.tm.exception.incorrect.IncorrectCommandException;
import ru.ekfedorov.tm.util.SystemUtil;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    public ICommandService commandService;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

    @Autowired
    public AbstractCommand[] commands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private void displayWelcome() {
        loggerService.debug("***           TEST          ***");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void init() {
        initPID();
        initCommands();
        initFileScanner();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    @SneakyThrows
    private void initCommands() {
        Arrays.stream(commands).forEach(cmd -> commandService.add(cmd));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    private boolean parseArgs(@Nullable final String... args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String param = args[0];
        parseArg(param);
        return true;
    }

    @SneakyThrows
    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new IncorrectCommandException(cmd);
        command.execute();
    }

    private void process() {
        while (true) {
            try {
                System.out.println();
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void run(final String... args) {
        displayWelcome();
        init();
        if (parseArgs(args)) new ExitCommand().execute();
        process();
    }

}

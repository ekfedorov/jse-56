package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ekfedorov.tm.endpoint.AdminEndpoint;
import ru.ekfedorov.tm.endpoint.Task;
import ru.ekfedorov.tm.endpoint.TaskEndpoint;
import ru.ekfedorov.tm.exception.system.NullTaskException;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

    protected void showTask(@Nullable final Task task) throws Exception {
        if (task == null) throw new NullTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().value());
        System.out.println("PROJECT: " + task.getProjectId());
    }

}

package ru.ekfedorov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.command.AbstractCommand;

@Component
public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Close application.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public void execute() {
        commandService.getCommandByName("logout").execute();
        System.exit(0);
    }

}

package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;

@Component
public final class ProjectClearAllCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Clear all project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[CLEAR]");
        adminEndpoint.clearProject(session);
        System.out.println("--- successfully cleared ---\n");
    }

}

package ru.ekfedorov.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractCommand;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display developer info.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "about";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}

package ru.ekfedorov.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-h";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display list of terminal commands.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getCommands();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command);
    }

}

package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Create new project.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-create";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "project";
        System.out.println("ENTER DESCRIPTION:");
        @Nullable String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        projectEndpoint.addProject(session, name, description);
        System.out.println();
    }

}
